# OpenML dataset: amazon-commerce-reviews

https://www.openml.org/d/1457

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Zhi Liu  
**Source**: UCI
**Please cite**:   

Dataset creator and donator: Zhi Liu, e-mail: liuzhi8673 '@' gmail.com, institution: National Engineering Research Center for E-Learning, Hubei Wuhan, China

Data Set Information:
 
dataset are derived from the customers reviews in Amazon Commerce Website for authorship identification. Most previous studies conducted the identification experiments for two to ten authors. But in the online context, reviews to be identified usually have more potential authors, and normally classification algorithms are not adapted to large number of target classes. To examine the robustness of classification algorithms, we identified 50 of the most active users (represented by a unique ID and username) who frequently posted reviews in these newsgroups. The number of reviews we collected for each author is 30.

Attribute Information:
 
attribution includes authors' linguistic style such as usage of digit, punctuation, words and sentences' length and usage frequency of words and so on

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1457) of an [OpenML dataset](https://www.openml.org/d/1457). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1457/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1457/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1457/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

